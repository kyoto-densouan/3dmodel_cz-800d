# README #

1/3スケールのSHARP X-1マニアタイプ用ディスプレイ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

組み込む液晶パネルは以下を想定しています。
http://akizukidenshi.com/catalog/g/gM-11967/
ＳＨＡＲＰ　５．５インチ高精細ＣＧシリコン液晶パネルセット　１０８０×１９２０ドット　ラズパイ用
メーカーカテゴリ　株式会社秋月電子通商

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-800d/raw/61fde01628c946dc941f35a44acc0043207daacb/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-800d/raw/61fde01628c946dc941f35a44acc0043207daacb/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-800d/raw/61fde01628c946dc941f35a44acc0043207daacb/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-800d/raw/61fde01628c946dc941f35a44acc0043207daacb/ExampleImage_2.jpg)
